#!/bin/bash

user_help()
{
echo "Please select one pattern from below
      1) Run as "./mongodb.sh install mongodb_version"
      2) Run as "./mongodb.sh uninstall mongodb_version"
      "
}

install()
{
version=$1

. /etc/os-release
OS=${NAME}

if [ -f /usr/bin/mongod ]; then
    echo "mongo db already installed."

elif [[ ${OS} == "Ubuntu" ]]; then
   #Import the public key used by the package management system.
   wget -qO - https://www.mongodb.org/static/pgp/server-${version}.asc | sudo apt-key add -

   #Create a list file for MongoDB.
   echo "deb [ arch=amd64,arm64 ] https://repo.mongodb.org/apt/ubuntu focal/mongodb-org/${version} multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-${version}.list

   #Reload local package database
   sudo apt-get update

   #Install the MongoDB packages
   sudo apt-get install -y mongodb-org

elif [[ ${OS} == "CentOS Linux" ]]; then

    #Configure the package management system
    [mongodb-org-${version}]
    name=MongoDB Repository
    baseurl=https://repo.mongodb.org/yum/redhat/$releasever/mongodb-org/${version}/x86_64/
    gpgcheck=1
    enabled=1
    gpgkey=https://www.mongodb.org/static/pgp/server-${version}.asc

    #Install the MongoDB packages
    sudo yum install -y mongodb-org

else
    echo "please give valid input:"
fi
}

uninstall()
{
version=$1

. /etc/os-release
OS=${NAME}

#stop mongodb
sudo service mongod stop

#Remove packages
if [[ ${OS} == "Ubuntu" ]]; then
   sudo apt-get purge mongodb-org*
   
elif [[ ${OS} == "CentOS Linux" ]]; then
   sudo yum erase $(rpm -qa | grep mongodb-org)  

else
  echo "No matches found"
fi

#Remove Data Directories
sudo rm -r /var/log/mongodb
sudo rm -r /var/lib/mongodb
}

