#!/bin/bash

source ./functionFile.sh 

function=$1
shift
case ${function} in
 "install") install "$@" ;;
 "uninstall") uninstall ;;
 "--help") user_help ;;
 *) echo "Invalid Input. Please visit help section with script name and pass --help"
esac
